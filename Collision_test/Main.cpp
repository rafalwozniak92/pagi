
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#include "main.h"										 


bool  g_bFullScreen = true;								 
HWND  g_hWnd;											 
RECT  g_rRect;											 
HDC   g_hDC;											 
HGLRC g_hRC;											 
HINSTANCE g_hInstance;									 

CVector3 g_vTriangle[3];
 
CVector3 g_vPosition;
 
float g_rotateY = 0;


void Init(HWND hWnd)
{
	g_hWnd = hWnd;										 
	GetClientRect(g_hWnd, &g_rRect);					 
	InitializeOpenGL(g_rRect.right, g_rRect.bottom);	 

	g_vTriangle[0] = CVector3(-1,  0,   0);
	g_vTriangle[1] = CVector3( 1,  0,   0);
	g_vTriangle[2] = CVector3( 0,  1,   0);
	g_Position = CVector3(0, 0.5f, 0);

}

bool AnimateNextFrame(int desiredFrameRate)
{
	static float lastTime = GetTickCount() * 0.001f;
	static float elapsedTime = 0.0f;

	float currentTime = GetTickCount() * 0.001f;  
	float deltaTime = currentTime - lastTime;  
	float desiredFPS = 1.0f / desiredFrameRate;  

	elapsedTime += deltaTime;  
	lastTime = currentTime;  

	 
	if( elapsedTime > desiredFPS )
	{
		elapsedTime -= desiredFPS;  

		 
		return true;
	}

	 
	return false;
}



WPARAM MainLoop()
{
	MSG msg;

	while(1)											 
	{													 
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{ 
			if(msg.message == WM_QUIT)					 
				break;
			TranslateMessage(&msg);						 
			DispatchMessage(&msg);						 
		}
		else											 
		{ 		
			if(AnimateNextFrame(60))					 
			{
				RenderScene();							 
			}
			else
			{
				Sleep(1);								 
			}
		} 
	}

	DeInit();											 

	return(msg.wParam);									 
}

void RenderScene() 
{
	int i=0;	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	 
	glLoadIdentity();									 

	gluLookAt(-2.5f, 0.5, -0.1,  0, 0.5f, 0,   0, 1, 0);  

	glRotatef(g_rotateY, 0, 1, 0);

	float radius = 0.1f;
	
	glBegin (GL_TRIANGLES);								 

		glColor3ub(255, 0, 0);							 
		glVertex3f(g_vTriangle[0].x, g_vTriangle[0].y, g_vTriangle[0].z);

		glColor3ub(0, 255, 0);							 
		glVertex3f(g_vTriangle[1].x, g_vTriangle[1].y, g_vTriangle[1].z);

		glColor3ub(0, 0, 255);							 
		glVertex3f(g_vTriangle[2].x, g_vTriangle[2].y, g_vTriangle[2].z);
	glEnd();											 

	GLUquadricObj *pObj = gluNewQuadric();				 

	 
	gluQuadricDrawStyle(pObj, GLU_LINE);				 

	 
	glTranslatef(g_vPosition.x, g_vPosition.y, g_vPosition.z);

	bool bCollided = SpherePolygonCollision(g_vTriangle, g_vPosition, 3, radius);  	
	
	 
	if(bCollided)
		glColor3ub(0, 255, 0);							 
	else		
		glColor3ub(255, 0, 255);						 
		
	 
	 
	gluSphere(pObj, radius, 15, 15);

	gluDeleteQuadric(pObj);								 

 


	SwapBuffers(g_hDC);									 
}

LRESULT CALLBACK WinProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    LONG    lRet = 0; 
    PAINTSTRUCT    ps;

    switch (uMsg)
	{ 
    case WM_SIZE:										 
		if(!g_bFullScreen)								 
		{
			SizeOpenGLScreen(LOWORD(lParam),HIWORD(lParam)); 
			GetClientRect(hWnd, &g_rRect);				 
		}
        break; 
 
	case WM_PAINT:										 
		BeginPaint(hWnd, &ps);							 
		EndPaint(hWnd, &ps);							 
		break;

	case WM_KEYDOWN:
		switch(wParam) 
		{
			case VK_ESCAPE:								 
				PostQuitMessage(0);						 
				break;

			case VK_UP:									 
				g_vPosition.y += 0.01f;					 
				break;

			case VK_DOWN:								 
				g_vPosition.y -= 0.01f;					 
				break;


			case VK_LEFT:								 
				g_vPosition.x -= 0.01f;					 
				break;

			case VK_RIGHT:								 
				g_vPosition.x += 0.01f;					 
				break;

			case VK_F3:									 
				g_vPosition.z -= 0.01f;					 
				break;

			case VK_F4:									 
				g_vPosition.z += 0.01f;					 
				break;

			case VK_F1:									 
				g_rotateY -= 2;							 
				break;

			case VK_F2:									 
				g_rotateY += 2;							 
				break;
		}

 

		break;

    case WM_CLOSE:										 
        PostQuitMessage(0);								 
        break; 
     
    default:											 
        lRet = DefWindowProc (hWnd, uMsg, wParam, lParam); 
        break; 
    } 
 
    return lRet;										 
}

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
//
