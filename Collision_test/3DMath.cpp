
#include "main.h"
#include <math.h>	 
#include <float.h>	 

float Absolute(float num)
{
	 
	 
	if(num < 0)
		return (0 - num);

	 
	return num;
}

 												
CVector3 Cross(CVector3 vVector1, CVector3 vVector2)
{
	CVector3 vNormal;									 

	 
	vNormal.x = ((vVector1.y * vVector2.z) - (vVector1.z * vVector2.y));
														
	 
	vNormal.y = ((vVector1.z * vVector2.x) - (vVector1.x * vVector2.z));
														
	 
	vNormal.z = ((vVector1.x * vVector2.y) - (vVector1.y * vVector2.x));

	return vNormal;										 
}


float Magnitude(CVector3 vNormal)
{
	
	return (float)sqrt( (vNormal.x * vNormal.x) + (vNormal.y * vNormal.y) + (vNormal.z * vNormal.z) );
}
 

CVector3 Normalize(CVector3 vNormal)
{
	float magnitude = Magnitude(vNormal);				
	vNormal.x /= magnitude;								 
	vNormal.y /= magnitude;								 
	vNormal.z /= magnitude;		
	return vNormal;										 
}


CVector3 Normal(CVector3 vPolygon[])					
{														 
	CVector3 vVector1 = vPolygon[2] - vPolygon[0];
	CVector3 vVector2 = vPolygon[1] - vPolygon[0];
	CVector3 vNormal = Cross(vVector1, vVector2);		 
	vNormal = Normalize(vNormal);		
	return vNormal;										 
}
								
float PlaneDistance(CVector3 Normal, CVector3 Point)
{	
	float distance = 0;									 
															 
	distance = - ((Normal.x * Point.x) + (Normal.y * Point.y) + (Normal.z * Point.z));

	return distance;									 
}
										
bool IntersectedPlane(CVector3 vPoly[], CVector3 vLine[], CVector3 &vNormal, float &originDistance)
{
	float distance1=0, distance2=0;						 
			
	vNormal = Normal(vPoly);							 

	 
	 
	originDistance = PlaneDistance(vNormal, vPoly[0]);

	 

	distance1 = ((vNormal.x * vLine[0].x)  +					 
		         (vNormal.y * vLine[0].y)  +					 
				 (vNormal.z * vLine[0].z)) + originDistance;	 
	
	 
	
	distance2 = ((vNormal.x * vLine[1].x)  +					 
		         (vNormal.y * vLine[1].y)  +					 
				 (vNormal.z * vLine[1].z)) + originDistance;	 

	 
	 
	 

	if(distance1 * distance2 >= 0)			 
	   return false;						 
					
	return true;							 
}
 

float Dot(CVector3 vVector1, CVector3 vVector2) 
{
		return ( (vVector1.x * vVector2.x) + (vVector1.y * vVector2.y) + (vVector1.z * vVector2.z) );
}


double AngleBetweenVectors(CVector3 Vector1, CVector3 Vector2)
{							
	 
	double dotProduct = Dot(Vector1, Vector2);				

	 
	double vectorsMagnitude = Magnitude(Vector1) * Magnitude(Vector2) ;

	 
	double angle = acos( dotProduct / vectorsMagnitude );

	 
	if(_isnan(angle))
		return 0;
	
	 
	return( angle );
}

											
CVector3 IntersectionPoint(CVector3 vNormal, CVector3 vLine[], double distance)
{
	CVector3 vPoint, vLineDir;					 
	double Numerator = 0.0, Denominator = 0.0, dist = 0.0;

	vLineDir = vLine[1] - vLine[0];		 
	vLineDir = Normalize(vLineDir);				 

	Numerator = - (vNormal.x * vLine[0].x +		 
				   vNormal.y * vLine[0].y +
				   vNormal.z * vLine[0].z + distance);

	 
	Denominator = Dot(vNormal, vLineDir);		 
	
	if( Denominator == 0.0)						 
		return vLine[0];						 

	dist = Numerator / Denominator;				 
	
	 
	vPoint.x = (float)(vLine[0].x + (vLineDir.x * dist));
	vPoint.y = (float)(vLine[0].y + (vLineDir.y * dist));
	vPoint.z = (float)(vLine[0].z + (vLineDir.z * dist));

	return vPoint;								 
}


bool InsidePolygon(CVector3 vIntersection, CVector3 Poly[], long verticeCount)
{
	const double MATCH_FACTOR = 0.99;		 
	double Angle = 0.0;						 
	CVector3 vA, vB;						 
	
	for (int i = 0; i < verticeCount; i++)		 
	{	
		vA = Poly[i] - vIntersection;			 
												 
		vB = Poly[(i + 1) % verticeCount] - vIntersection;
												
		Angle += AngleBetweenVectors(vA, vB);	 
	}
											
	if(Angle >= (MATCH_FACTOR * (2.0 * PI)) )	 
		return true;							 
		
	return false;								 
}

 

bool IntersectedPolygon(CVector3 vPoly[], CVector3 vLine[], int verticeCount)
{
	CVector3 vNormal;
	float originDistance = 0;

	 
									  
	if(!IntersectedPlane(vPoly, vLine,   vNormal,   originDistance))
		return false;

	 
	 
	CVector3 vIntersection = IntersectionPoint(vNormal, vLine, originDistance);

	 
	if(InsidePolygon(vIntersection, vPoly, verticeCount))
		return true;							 

	return false;								 
}


float Distance(CVector3 vPoint1, CVector3 vPoint2)
{
	
	double distance = sqrt( (vPoint2.x - vPoint1.x) * (vPoint2.x - vPoint1.x) +
						    (vPoint2.y - vPoint1.y) * (vPoint2.y - vPoint1.y) +
						    (vPoint2.z - vPoint1.z) * (vPoint2.z - vPoint1.z) );

	 
	return (float)distance;
}



CVector3 ClosestPointOnLine(CVector3 vA, CVector3 vB, CVector3 vPoint)
{
	 
	CVector3 vVector1 = vPoint - vA;

	 
    CVector3 vVector2 = Normalize(vB - vA);

	 
    float d = Distance(vA, vB);

	 
	 
    float t = Dot(vVector2, vVector1);

	 
	 
    if (t <= 0) 
		return vA;

	 
	 
    if (t >= d) 
		return vB;
 
	 
    CVector3 vVector3 = vVector2 * t;

	 
	 
    CVector3 vClosestPoint = vA + vVector3;

	 
	return vClosestPoint;
}


 

bool SpherePolygonCollision(CVector3 vPolygon[], 
							CVector3 &vCenter, int vertexCount, float radius)
{
	
	 
	CVector3 vNormal = Normal(vPolygon);

	float distance = 0.0f;

	int classification = ClassifySphere(vCenter, vNormal, vPolygon[0], radius, distance);

	if(classification == INTERSECTS) 
	{
		
		CVector3 vOffset = vNormal * distance;

		CVector3 vPosition = vCenter - vOffset;

		if(InsidePolygon(vPosition, vPolygon, vertexCount))
			return true;	 
		else
		{
		
			if(EdgeSphereCollision(vCenter, vPolygon, vertexCount, radius))
			{
				return true;	 
			}
		}
	}

	 
	return false;
}



int ClassifySphere(CVector3 &vCenter, 
				   CVector3 &vNormal, CVector3 &vPoint, float radius, float &distance)
{	 
	 
	float d = (float)PlaneDistance(vNormal, vPoint);

	distance = (vNormal.x * vCenter.x + vNormal.y * vCenter.y + vNormal.z * vCenter.z + d);

	
	if(Absolute(distance) < radius)
		return INTERSECTS;
	 
	 
	else if(distance >= radius)
		return FRONT;
	
	 
	return BEHIND;
}



bool EdgeSphereCollision(CVector3 &vCenter, 
						 CVector3 vPolygon[], int vertexCount, float radius)
{
	CVector3 vPoint;

	for(int i = 0; i < vertexCount; i++)
	{
	
		vPoint = ClosestPointOnLine(vPolygon[i], vPolygon[(i + 1) % vertexCount], vCenter);		
		float distance = Distance(vPoint, vCenter);	
		if(distance < radius)
			return true;
	}

	 
	return false;
}

 