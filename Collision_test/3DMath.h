#ifndef _3DMATH_H
#define _3DMATH_H

#define PI 3.1415926535897932	
#define BEHIND		0	 
#define INTERSECTS	1	 
#define FRONT		2	 
struct CVector3
{
public:
	
	 
	CVector3() {}

	 
	CVector3(float X, float Y, float Z) 
	{ 
		x = X; y = Y; z = Z;
	}

	 
	CVector3 operator+(CVector3 vVector)
	{
		 
		return CVector3(vVector.x + x, vVector.y + y, vVector.z + z);
	}

	 
	CVector3 operator-(CVector3 vVector)
	{
		 
		return CVector3(x - vVector.x, y - vVector.y, z - vVector.z);
	}
	
	 
	CVector3 operator*(float num)
	{
		 
		return CVector3(x * num, y * num, z * num);
	}

	float x, y, z;						
};


 
CVector3 Cross(CVector3 vVector1, CVector3 vVector2);

 
float Magnitude(CVector3 vNormal);

 
CVector3 Normalize(CVector3 vNormal);

 
CVector3 Normal(CVector3 vPolygon[]);

 
float Distance(CVector3 vPoint1, CVector3 vPoint2);

 
CVector3 ClosestPointOnLine(CVector3 vA, CVector3 vB, CVector3 vPoint);

 
 
float PlaneDistance(CVector3 Normal, CVector3 Point);

 
bool IntersectedPlane(CVector3 vPoly[], CVector3 vLine[], CVector3 &vNormal, float &originDistance);

 
float Dot(CVector3 vVector1, CVector3 vVector2);

 
double AngleBetweenVectors(CVector3 Vector1, CVector3 Vector2);

 
CVector3 IntersectionPoint(CVector3 vNormal, CVector3 vLine[], double distance);

 
bool InsidePolygon(CVector3 vIntersection, CVector3 Poly[], long verticeCount);

 
bool IntersectedPolygon(CVector3 vPoly[], CVector3 vLine[], int verticeCount);
 
float absolute(float num); 
 
int ClassifySphere(CVector3 &vCenter, 
				   CVector3 &vNormal, CVector3 &vPoint, float radius, float &distance); 
 
bool EdgeSphereCollision(CVector3 &vCenter, 
						 CVector3 vPolygon[], int vertexCount, float radius); 
 
bool SpherePolygonCollision(CVector3 vPolygon[], 
							CVector3 &vCenter, int vertexCount, float radius);

#endif 


 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 


