#include "Globals.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

SDL_Window* glWindow = nullptr;
SDL_GLContext glContext;

GLuint sceneProgram;
GLuint selectProgram;

GLuint verticesBuffer;
GLuint uvsBuffer;
GLuint normalsBuffer;

GLuint sceneVerticesBuffer;
GLuint sceneUvsBuffer;

GLuint indicesBuffer;

GLuint diffuseTex;

GLuint frameBuffer;

GLuint sceneTexture;
GLuint selectColorTexture; 
GLuint depthTexture;

mat4 vpMatrix;

GLfloat sceneVertices[] =
{
	-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f
};
GLfloat sceneUVs[] =
{
	0.0f, 0.0f,
	1.0f, 0.0f,
	1.0f, -1.0f,
	0.0f, -1.0f
};

Object3D objects[3];

Object3D* selected = nullptr;

vec2 mousePosition;
bool isLMBdown;

bool isLAdown;
bool isRAdown;
bool isUAdown;
bool isDAdown;

bool InitGL()
{
	/////////	SHADERS FOR SCENE PLANE		/////
	//Create program for shaders
	sceneProgram = glCreateProgram();
	if (!glIsProgram(sceneProgram))
	{
		cout << "program\n";
		return false;
	}

	//Create shaders
	GLuint vs_shdr = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs_shdr, 1, vsSimple, NULL);
	GLuint fs_shdr = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs_shdr, 1, fsSimple, NULL);

	//Compile shaders
	glCompileShader(vs_shdr);
	glCompileShader(fs_shdr);

	GLint status;

	//Validate compilation
	glGetShaderiv(vs_shdr, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "vs\n";
		return false;
	}
	glGetShaderiv(fs_shdr, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "fs\n";
		return false;
	}

	//Attach shaders to program
	glAttachShader(sceneProgram, vs_shdr);
	glAttachShader(sceneProgram, fs_shdr);

	//Link program
	glLinkProgram(sceneProgram);

	//Validate linking
	glGetProgramiv(sceneProgram, GL_LINK_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "link\n";
		return false;
	}

	///////////////////		SHADERS FOR MAIN SCENE		/////////////////////////////////

	selectProgram = glCreateProgram();
	if (!glIsProgram(selectProgram))
	{
		cout << "program\n";
		return false;
	}

	vs_shdr = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs_shdr, 1, vsSelect, NULL);
	fs_shdr = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs_shdr, 1, fsSelect, NULL);

	glCompileShader(vs_shdr);
	glCompileShader(fs_shdr);

	glGetShaderiv(vs_shdr, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "vs\n";
		return false;
	}
	glGetShaderiv(fs_shdr, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "fs\n";
		return false;
	}

	glAttachShader(selectProgram, vs_shdr);
	glAttachShader(selectProgram, fs_shdr);

	glLinkProgram(selectProgram);

	glGetProgramiv(selectProgram, GL_LINK_STATUS, &status);
	if (status != GL_TRUE)
	{
		cout << "link\n";
		return false;
	}

	return true;
}

bool Init()
{
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		glWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if (glWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			glContext = SDL_GL_CreateContext(glWindow);
			if (glContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!InitGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	//Init SDL_image
	if (IMG_Init(IMG_INIT_PNG) == 0)
	{
		printf("Unable to initialize IMG!\n");
		success = false;
	}

	InitObjects();

	return success;
}

//Set objects property
void InitObjects()
{
	objects[0].selectColor = vec3(0.0f, 0.0f, 1.0f);

	objects[1].selectColor = vec3(0.0f, 1.0f, 0.0f);
	objects[1].transform.SetParent(&objects[0].transform);
	objects[1].transform.Translate(vec3(3.f, 0.f, 0.f));

	objects[2].selectColor = vec3(1.0f, 0.0f, 0.0f);
	objects[2].transform.SetParent(&objects[1].transform);
	objects[2].transform.SetLocalPosition(vec3(0.f, 0.f, 0.f));
	objects[2].transform.Translate(vec3(0.f, 3.f, 0.f));
}

//Clean up
void Close()
{
	//Deallocate program
	glDeleteProgram(sceneProgram);
	glDeleteProgram(selectProgram);
	glDeleteBuffers(1, &verticesBuffer);
	glDeleteBuffers(1, &uvsBuffer);
	glDeleteBuffers(1, &normalsBuffer);
	glDeleteBuffers(1, &indicesBuffer);

	glDeleteBuffers(1, &sceneVerticesBuffer);
	glDeleteBuffers(1, &sceneUvsBuffer);

	glDeleteFramebuffers(1, &frameBuffer);

	glDeleteTextures(1, &diffuseTex);
	glDeleteTextures(1, &sceneTexture);
	glDeleteTextures(1, &selectColorTexture);

	glUseProgram(0);
	//Destroy window	
	SDL_DestroyWindow(glWindow);
	glWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

void SetModel(const Model& model, const string& texturePath)
{
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);		//Set viewport for rendering
	glEnable(GL_DEPTH_TEST);							//enable depth test
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				//set clear color to black

	glGenBuffers(1, &verticesBuffer);					
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * model.VerticesSize(), model.verticesData, GL_STATIC_DRAW);

	glGenBuffers(1, &uvsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * model.UvsSize(), model.uvsData, GL_STATIC_DRAW);

	glGenBuffers(1, &normalsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * model.NormalsSize(), model.normalsData, GL_STATIC_DRAW);

	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * model.IndicesSize(), model.indicesData, GL_STATIC_DRAW);

	glGenBuffers(1, &sceneVerticesBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, sceneVerticesBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sceneVertices), sceneVertices, GL_STATIC_DRAW);

	glGenBuffers(1, &sceneUvsBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, sceneUvsBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sceneUVs), sceneUVs, GL_STATIC_DRAW);

	SDL_Surface* diffuse = IMG_Load(texturePath.c_str());
	if (diffuse == NULL)
		printf("Can't read img\n");

	glGenTextures(1, &diffuseTex);
	glBindTexture(GL_TEXTURE_2D, diffuseTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, diffuse->w, diffuse->h, 0, GL_BGR, GL_UNSIGNED_BYTE, diffuse->pixels);

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	SDL_FreeSurface(diffuse);

	//Create Framebuffer

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &sceneTexture);
	glBindTexture(GL_TEXTURE_2D, sceneTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &selectColorTexture);
	glBindTexture(GL_TEXTURE_2D, selectColorTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Set textures to render targets

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, selectColorTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthTexture, 0);

	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, buffers);
}

void SetMatrices()
{
	mat4 pMatrix = perspective(45.f, ((float)SCREEN_WIDTH) / SCREEN_HEIGHT, 0.1f, 50.f);
	mat4 vMatrix = translate(mat4(), vec3(0.f, 0.f, -50.1f / 2.f));
	mat4 scaleMat = scale(mat4(), vec3(2.0f));
	vpMatrix = pMatrix * vMatrix * scaleMat;
}

void Draw(const Model& model)
{

	///////////////////////////////////////////////////////////////////////////////////////////////////
	glUseProgram(selectProgram);

	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, verticesBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvsBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, diffuseTex);
	glUniform1i(glGetUniformLocation(selectProgram, "diffuse"), 0);

	for (int i = 0; i < 3; ++i)
	{
		glUniformMatrix4fv(glGetUniformLocation(selectProgram, "mvpMatrix"), 1, GL_FALSE, value_ptr(vpMatrix * objects[i].transform.GetModelMatrix()));
		glUniform3f(glGetUniformLocation(selectProgram, "select"), objects[i].selectColor.r, objects[i].selectColor.g, objects[i].selectColor.b);
		if (objects[i].isSelected)
			glUniform3f(glGetUniformLocation(selectProgram, "selected"), 0.3f, 0.3f, 0.0f);
		else
			glUniform3f(glGetUniformLocation(selectProgram, "selected"), 0.0f, 0.0f, 0.0f);

		glDrawElements(GL_TRIANGLES, model.IndicesSize(), GL_UNSIGNED_INT, NULL);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	glUseProgram(sceneProgram);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, sceneVerticesBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, sceneUvsBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sceneTexture);
	glUniform1i(glGetUniformLocation(sceneProgram, "scene"), 0);

	glDrawArrays(GL_QUADS, 0, 4);
}

void Update()
{
	objects[0].transform.Rotate(vec3(0.f, 1.f, 0.f), 3.14f / 2.f / 10.f / 9.f);
	objects[1].transform.Rotate(vec3(1.f, 0.f, 0.f), 3.14f / 2.f / 10.f / 9.f / 2.f);

	if (selected)
	{
		if (isUAdown)
			selected->transform.Translate(vec3(0.f, 1.f, 0.f));
		if (isDAdown)
			selected->transform.Translate(vec3(0.f, -1.f, 0.f));
		if (isRAdown)
			selected->transform.Translate(vec3(1.f, 0.f, 0.f));
		if (isLAdown)
			selected->transform.Translate(vec3(-1.f, 0.f, 0.f));
	}

	if (!isLMBdown)
		return;

	glBindFramebuffer(GL_READ_FRAMEBUFFER, frameBuffer);
	glReadBuffer(GL_COLOR_ATTACHMENT1);

	unsigned char pixel[3];

	glReadPixels((int)mousePosition.x, SCREEN_HEIGHT - (int)mousePosition.y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

	if (selected)
	{
		selected->isSelected = false;
		selected = nullptr;
	}

	bool isR = (int)pixel[0] > 0;
	bool isG = (int)pixel[1] > 0;
	bool isB = (int)pixel[2] > 0;

	for (int i = 0; i < 3; ++i)
	{
		if (isR && objects[i].selectColor.r > 0)
		{
			selected = &objects[i];
			objects[i].isSelected = true;
			break;
		}
		if (isG && objects[i].selectColor.g > 0)
		{
			selected = &objects[i];
			objects[i].isSelected = true;
			break;
		}
		if (isB && objects[i].selectColor.b > 0)
		{
			selected = &objects[i];
			objects[i].isSelected = true;
			break;
		}
	}
}