#pragma once

#include "Headers.h"
#include "Shaders.h"

extern const int SCREEN_WIDTH;		
extern const int SCREEN_HEIGHT;

extern SDL_Window* glWindow;
extern SDL_GLContext glContext;

extern GLuint sceneProgram;
extern GLuint selectProgram;

//Buffers for main scene
extern GLuint verticesBuffer;		//pointer to buffer with vertices
extern GLuint uvsBuffer;			//pointer to buffer with uvs
extern GLuint normalsBuffer;		//pointer to buffer with normals

extern GLuint indicesBuffer;

//Buffers for plane
extern GLuint sceneVerticesBuffer;
extern GLuint sceneUvsBuffer;

extern GLuint diffuseTex;			//pointer to texture with box diffuse

extern GLuint frameBuffer;			//pointer for framebuffer with scene and color to selection

extern GLuint sceneTexture;			//pointer to texture with main scene
extern GLuint selectColorTexture;	//pointer to texture with color to selection
extern GLuint depthTexture;			//texture for depth buffer

extern mat4 vpMatrix;				//projection x view

extern GLfloat sceneVertices[];		//array with plane vertices
extern GLfloat sceneUVs[];			//array with plane uvs

extern Object3D objects[3];			//array with objects on scene

extern Object3D* selected;			//selected object

extern vec2 mousePosition;			
extern bool isLMBdown;

extern bool isLAdown;				//left arrow
extern bool isRAdown;				//right arrow
extern bool isUAdown;				//up arrow
extern bool isDAdown;				//down arrow

extern bool InitGL();
extern bool Init();
extern void InitObjects();
extern void Close();

extern void SetMatrices();

extern void SetModel(const Model& model, const string& texturePath);

extern void Draw(const Model& model);

extern void Update();