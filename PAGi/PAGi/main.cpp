#include "Headers.h"
#include "Globals.h"

int main(int argc, char* args[])
{
	Model box;
	if (argc > 1)
	{
		Init();
		box.Load("C:/Users/RafaU/Desktop/Semestr VI/PAG/PAGi/Debug/cube");
		SetModel(box, "C:/Users/RafaU/Desktop/Semestr VI/PAG/PAGi/Debug/cube_diffuse");
		SetMatrices();
	}
	else
	{
		InitObjects();
		cout << "Use console!\n";
		getchar();
		return 0;
	}

	bool run = true;
	SDL_Event event;

	while (run)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				run = false;

			if (event.type == SDL_MOUSEMOTION)
			{
				int x, y;
				SDL_GetMouseState(&x, &y);
				mousePosition.x = x;
				mousePosition.y = y;
			}
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_LEFT)
					isLMBdown = true;
			}

			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_UP:
					isUAdown = true;
					break;
				case SDLK_DOWN:
					isDAdown = true;
					break;
				case SDLK_RIGHT:
					isRAdown = true;
					break;
				case SDLK_LEFT:
					isLAdown = true;
					break;
				}
			}
		}

		Draw(box);
		SDL_GL_SwapWindow(glWindow);
		glFlush();

		Update();

		isLMBdown = false;

		isLAdown = false;
		isRAdown = false;
		isUAdown = false;
		isDAdown = false;
	}
	Close();

	return 0;
}