#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/quaternion.hpp>
#include <gtx/quaternion.hpp>
#include <gtc/type_ptr.hpp>

#include <SDL.h>
#include <SDL_image.h>
#include <glew.h>
#include <SDL_opengl.h>

using namespace std;
using namespace glm;

#include "Transform.h"
#include "Model.h"
#include "Object3D.h"
