#pragma once
#include "Headers.h"

class Model
{
protected:
	vector<GLfloat> vertices;
	vector<GLfloat> uvs;
	vector<GLfloat> normals;
	vector<GLuint> indices;

public:
	GLfloat* verticesData;
	GLfloat* uvsData;
	GLfloat* normalsData;
	GLuint* indicesData;

	Model();
	Model(const string& path);
	void Load(const string& path);

	int VerticesSize() const;
	int UvsSize() const;
	int NormalsSize() const;

	int IndicesSize() const;
};

