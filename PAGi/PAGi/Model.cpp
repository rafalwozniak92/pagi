#include "Model.h"

Model::Model()
{

}

Model::Model(const string& path)
{
	Load(path);
}

void Model::Load(const string& path)
{

	vector<GLfloat> tmp_vertices;
	vector<GLfloat> tmp_uvs;
	vector<GLfloat> tmp_normals;

	vector<GLuint> tmp_vertices_indices;
	vector<GLuint> tmp_uvs_indices;
	vector<GLuint> tmp_normals_indices;

	fstream modelFile;
	modelFile.open(path);
	if (!modelFile.good())
	{
		cout << "Error: Load file" << endl;
		return;
	}

	string fileLine;
	istringstream lineStream;
	string value;
	while (getline(modelFile, fileLine))
	{
		lineStream.clear();
		value.clear();

		if (!fileLine.empty())
			fileLine.pop_back();

		lineStream.str(fileLine);
		lineStream >> value;
		
		if (value == "v")
		{
			float tmpVertex;

			lineStream >> tmpVertex;
			tmp_vertices.push_back(tmpVertex);
			lineStream >> tmpVertex;
			tmp_vertices.push_back(tmpVertex);
			lineStream >> tmpVertex;
			tmp_vertices.push_back(tmpVertex);
		}
		if (value == "vt")
		{
			float tmpUV;

			lineStream >> tmpUV;
			tmp_uvs.push_back(tmpUV);
			lineStream >> tmpUV;
			tmp_uvs.push_back(-tmpUV);
		}
		if (value == "vn")
		{
			float tmpNormal;

			lineStream >> tmpNormal;
			if (tmpNormal >= -0.0001f && tmpNormal <= 0.0001f)
				tmpNormal = 0.0f;
			tmp_normals.push_back(tmpNormal);
			lineStream >> tmpNormal;
			if (tmpNormal >= -0.0001f && tmpNormal <= 0.0001f)
				tmpNormal = 0.0f;
			tmp_normals.push_back(tmpNormal);
			lineStream >> tmpNormal;
			if (tmpNormal >= -0.0001f && tmpNormal <= 0.0001f)
				tmpNormal = 0.0f;
			tmp_normals.push_back(tmpNormal);
		}
		if (value == "f")
		{
			while (!lineStream.eof())
			{
				unsigned int tmpIndex;

				lineStream >> tmpIndex;
				tmp_vertices_indices.push_back(tmpIndex - 1);
				lineStream.seekg(1, ios_base::cur);
				lineStream >> tmpIndex;
				tmp_uvs_indices.push_back(tmpIndex - 1);
				lineStream.seekg(1, ios_base::cur);
				lineStream >> tmpIndex;
				tmp_normals_indices.push_back(tmpIndex - 1);
			}
		}
	}

	const unsigned int verticesCount = tmp_vertices_indices.size();
	for (int i = 0; i < verticesCount; ++i)
	{
		unsigned int index = uvs.size() / 2;

		const unsigned int indicesCount = indices.size();
		for (int j = 0; j < i && j < indicesCount; ++j)
		{
			const bool isVertexEqual = 
				vertices[(indices[j] * 3)] == tmp_vertices[(tmp_vertices_indices[i] * 3)] &&
				vertices[(indices[j] * 3) + 1] == tmp_vertices[(tmp_vertices_indices[i] * 3) + 1] &&
				vertices[(indices[j] * 3) + 2] == tmp_vertices[(tmp_vertices_indices[i] * 3) + 2];
			const bool isUvEqual =
				uvs[(indices[j] * 2)] == tmp_uvs[(tmp_uvs_indices[i] * 2)] &&
				uvs[(indices[j] * 2) + 1] == tmp_uvs[(tmp_uvs_indices[i] * 2) + 1];
			const bool isNormalEqual =
				normals[(indices[j] * 3)] == tmp_normals[(tmp_normals_indices[i] * 3)] &&
				normals[(indices[j] * 3) + 1] == tmp_normals[(tmp_normals_indices[i] * 3) + 1] &&
				normals[(indices[j] * 3) + 2] == tmp_normals[(tmp_normals_indices[i] * 3) + 2];

			if (isVertexEqual && isUvEqual && isNormalEqual)
			{
				index = indices[j];
				break;
			}
		}

		if (index == uvs.size() / 2)
		{
			vertices.push_back(tmp_vertices[(tmp_vertices_indices[i] * 3)]);
			vertices.push_back(tmp_vertices[(tmp_vertices_indices[i] * 3) + 1]);
			vertices.push_back(tmp_vertices[(tmp_vertices_indices[i] * 3) + 2]);

			uvs.push_back(tmp_uvs[(tmp_uvs_indices[i] * 2)]);
			uvs.push_back(tmp_uvs[(tmp_uvs_indices[i] * 2) + 1]);

			vec3 normal(
				tmp_normals[(tmp_normals_indices[i] * 3)],
				tmp_normals[(tmp_normals_indices[i] * 3) + 1],
				tmp_normals[(tmp_normals_indices[i] * 3) + 2]
				);

			normal = normalize(normal);

			normals.push_back(normal.x);
			normals.push_back(normal.y);
			normals.push_back(normal.z);
		}

		indices.push_back(index);
	}

	modelFile.close();

	verticesData = vertices.data();
	uvsData = uvs.data();
	normalsData = normals.data();

	indicesData = indices.data();
}

int Model::VerticesSize() const
{
	return vertices.size();
}
int Model::UvsSize() const
{
	return uvs.size();
}
int Model::NormalsSize() const
{
	return normals.size();
}
int Model::IndicesSize() const
{
	return indices.size();
}