#include "Shaders.h"


const char* vsSimple[]
{
	"#version 330										\n"

	"layout(location=0) in vec4 inPosition;				\n"
	"layout(location=1) in vec2 inUV;					\n"

	"out vec2 outUV;									\n"

	"void main()										\n"
	"{													\n"
	"	gl_Position = inPosition;						\n"
	"	outUV = inUV;									\n"
	"}													\n"
};
const char* fsSimple[]
{
	"#version 330										\n"

	"uniform sampler2D scene;							\n"
	"in vec2 outUV;										\n"
	"out vec4 outColor;									\n"

	"void main()										\n"
	"{													\n"
	"	outColor = texture(scene, outUV);				\n"
	"}													\n"
};

const char* vsSelect[]
{
	"#version 330										\n"

	"layout(location=0) in vec4 inPosition;				\n"
	"layout(location=1) in vec2 inUV;					\n"

	"uniform mat4 mvpMatrix;							\n"

	"out vec2 outUV;									\n"

	"void main()										\n"
	"{													\n"
	"	gl_Position = mvpMatrix * inPosition;			\n"
	"	outUV = inUV;									\n"
	"}													\n"
};
const char* fsSelect[]
{
	"#version 330																	\n"

	"layout(location=0) out vec4 diffuseColor;										\n"
	"layout(location=1) out vec4 selectColor;										\n"

	"uniform sampler2D diffuse;														\n"
	"in vec2 outUV;																	\n"
	"uniform vec3 select;															\n"
	"uniform vec3 selected;															\n"

	"void main()																	\n"
	"{																				\n"
	"	diffuseColor = texture(diffuse, outUV) + vec4(selected.xyz, 1.0f);			\n"
	"	selectColor = vec4(select.xyz, 1.0f);										\n"
	"}																				\n"
};